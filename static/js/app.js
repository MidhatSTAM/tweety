$('.alert-dismissible').delay(2500).fadeOut();

function key_down(e) {
    if (e.keyCode === 13) {
        e.preventDefault();
        search();
    }
}

function search() {
    if ($('.search-input') !== '') {
        $('.submit-button').click();
    }
}

$('.card-text > a').attr("target", "_blank");

$('.slide-down').click(function () {
    $('.schedule-cont').toggle();
});