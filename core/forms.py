from django import forms
from core.models import VisualizedData


class VisualizedDataForm(forms.ModelForm):
    class Meta:
        model = VisualizedData
        exclude = (
            'visualize_data_searched_query',
            'visualize_data_date',
            'visualize_data_image',
            'visualize_data_user',
        )
