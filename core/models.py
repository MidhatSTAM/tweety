from django.db import models

# Create your models here.


class VisualizedData(models.Model):
	class Meta:
		db_table = "visualize_data"
	
	visualize_data_searched_query = models.TextField()
	visualize_data_image = models.CharField(max_length=1000)
	visualize_data_date = models.DateTimeField(auto_now_add=True)
	visualize_data_user = models.CharField(max_length=255)
