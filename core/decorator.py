from django.http import HttpResponseRedirect


def my_login_required(function):
	def wrapper(request, *args, **kw):
		try:
			twitter_token = request.COOKIES['twitter_token']
		except:
			twitter_token = None
		if not twitter_token:
			return HttpResponseRedirect('/login/')
		else:
			return function(request, *args, **kw)
	return wrapper
