import json
import re

import pandas as pd
import numpy as np
import tweepy
import time
import schedule
from django.core.paginator import Paginator

from django.http import HttpResponseRedirect

from core.decorator import my_login_required
from birdy.twitter import UserClient
from django.shortcuts import render, redirect

from core.models import VisualizedData
from core.forms import VisualizedDataForm

from wordcloud import WordCloud

from apscheduler.schedulers.background import BackgroundScheduler

CONSUMER_KEY = 'U2EGqeS3aHj1Pc0qF9VgyEH29'
CONSUMER_SECRET = '8ktHEcBWQWF2X53nH1mkUUL3KEa2NXjiPXB0E3GMzCYJCSKdV6'
CALLBACK_URL = 'http://127.0.0.1:8010/login/twoauth2callback'


@my_login_required
def home_page(request, page=1):
    global access_token
    global access_token_secret
    # global api

    args = {}

    access_token = request.COOKIES['access_token']
    access_token_secret = request.COOKIES['access_token_secret']

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    args['name'] = api.me().name
    args['image'] = api.me().profile_image_url_https
    args['tweets'] = api.me().statuses_count
    args['followings'] = api.me().friends_count
    args['followers'] = api.me().followers_count
    args['likes'] = api.me().favourites_count

    queryset = api.home_timeline(tweet_mode='extended', page=page)

    paginator = Paginator(queryset, 10)
    try:
        query_list = paginator.page(page)
    except:
        query_list = paginator.page(1)

    args['page'] = page
    args['home_timeline'] = query_list

    if int(page) > 1:
        args['previous_page_number'] = str(int(page) - 1)
    else:
        args['previous_page_number'] = None
    args['next_page_number'] = str(int(page) + 1)

    args['path_name'] = 'Homepage'
    return render(request, 'main/home.html', args)


@my_login_required
def search(request):
    args = {}

    if request.POST.get("search_input") is None:
        return home_page(request)
    else:
        access_token = request.COOKIES['access_token']
        access_token_secret = request.COOKIES['access_token_secret']

        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(access_token, access_token_secret)

        api = tweepy.API(auth)

        args['name'] = api.me().name
        args['image'] = api.me().profile_image_url_https
        args['tweets'] = api.me().statuses_count
        args['followings'] = api.me().friends_count
        args['followers'] = api.me().followers_count
        args['likes'] = api.me().favourites_count

        args['lookup_tweets'] = lookup_tweets = api.search(
            q=str(request.POST.get('search_input')), tweet_mode='extended'
        )

        if request.POST.get('save_search'):
            search_term = request.POST.get('search_input')

            data = [status._json for status in lookup_tweets]

            with open('static/json_files/' + str(int(time.time())) + '.json', 'w') as file:
                json.dump(data, file)

            analyze = TweetAnalyzer().tweets_to_data_frame(lookup_tweets)

            analyze['clean_tweet'] = analyze['tweets'].apply(lambda x: clean_tweet(x))
            all_tweets = ' '.join(tweet for tweet in analyze['clean_tweet'])
            wordcloud = WordCloud(max_font_size=50, max_words=100, background_color="white").generate(all_tweets)
            wordcloud.to_file('static/img/data/' + str(int(time.time())) + '.jpg')

            visualize_save_form = VisualizedDataForm(request.POST)
            if visualize_save_form.is_valid():
                new_visualized_data = visualize_save_form.save(commit=False)
                new_visualized_data.visualize_data_searched_query = search_term
                new_visualized_data.visualize_data_image = 'static/img/data/' + str(int(time.time())) + '.jpg'
                new_visualized_data.visualize_data_user = str(api.me().id_str)
                new_visualized_data.save()

                return visualize(request)

        args['path_name'] = 'Search'
        args['searched_term'] = str(request.POST.get('search_input')) or ''
        return render(request, 'main/home.html', args)


def clean_tweet(tweet):
    return ' '.join(re.sub('(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)|(RT)', ' ', tweet).split())


@my_login_required
def visualize(request):
    args = {}

    access_token = request.COOKIES['access_token']
    access_token_secret = request.COOKIES['access_token_secret']

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    args['name'] = api.me().name
    args['image'] = api.me().profile_image_url_https

    my_id = api.me().id_str

    args['v_data'] = VisualizedData.objects.filter(
        visualize_data_user=my_id
    )
    return render(request, 'main/visualize.html', args)


@my_login_required
def schedule_page(request):
    args = {}

    access_token = request.COOKIES['access_token']
    access_token_secret = request.COOKIES['access_token_secret']

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    args['name'] = api.me().name
    args['image'] = api.me().profile_image_url_https

    return render(request, 'main/schedule.html', args)


def post_tweet(access_token, access_token_secret, tweet_body):
    access_token = access_token
    access_token_secret = access_token_secret
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    tweet_body = tweet_body
    api.update_status(status=tweet_body)


@my_login_required
def new_tweet(request):
    args = {}

    access_token = request.COOKIES['access_token']
    access_token_secret = request.COOKIES['access_token_secret']

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    args['name'] = api.me().name
    args['image'] = api.me().profile_image_url_https

    if request.POST.get('schedule_at'):
        schedule_at = request.POST.get('schedule_at')
        repeat = request.POST.get('custom-radio-2')
        tweet_body = request.POST.get('tweet_body')
        print(repeat)
        if repeat == "day":
            # with open('schedule_at.py', 'w') as schedule_at_file:
            #     importing_lines = "import schedule\nimport time\nimport tweepy\n\n\n"
            #
            #     accessing_tokens = "CONSUMER_KEY = 'U2EGqeS3aHj1Pc0qF9VgyEH29'\n" + \
            #                        "CONSUMER_SECRET = '8ktHEcBWQWF2X53nH1mkUUL3KEa2NXjiPXB0E3GMzCYJCSKdV6'\n" + \
            #                        "CALLBACK_URL = 'http://127.0.0.1:8010/login/twoauth2callback'\n\n"
            #
            #     post_tweet_job = "def post_tweet():\n" + "\taccess_token = '" + str(access_token) + "'\n" + \
            #                      "\taccess_token_secret = '" + str(access_token_secret) + "'\n" + \
            #                      "\tauth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)\n" + \
            #                      "\tauth.set_access_token(access_token, access_token_secret)\n" + \
            #                      "\tapi = tweepy.API(auth)\n" + "\ttweet_body = '" + str(tweet_body) + "'\n" + \
            #                      "\tapi.update_status(status=tweet_body)\n"
            #     scheduling = "schedule.every()." + str(repeat) + '.at("' + str(schedule_at) + '").do(post_tweet)\n\n'
            #
            #     run_scr = "while True:\n" + \
            #               "\tschedule.run_pending()\n" + \
            #               "\ttime.sleep(1)"
            #
            #     schedule_at_file.write(importing_lines + accessing_tokens + post_tweet_job + scheduling + run_scr)
            #
            # subprocess.Popen(['python', 'schedule_at.py'])

            # schedule.every().day.at(str(schedule_at)).do(post_tweet, access_token, access_token_secret, tweet_body)
            # schedule.run_pending()

            scheduling = BackgroundScheduler()
            scheduling.add_job(post_tweet, access_token, access_token_secret, tweet_body, "cron", hour=0)

        return render(request, "main/home.html", args)

    elif request.POST.get('schedule_for'):
        pass
    else:
        tweet_body = request.POST.get('tweet_body')
        api.update_status(status=tweet_body)
    return render(request, 'main/home.html', args)


class TweetAnalyzer():

    def tweets_to_data_frame(self, tweets):
        df = pd.DataFrame(data=[tweet.full_text for tweet in tweets], columns=['tweets'])

        df['id'] = np.array([tweet.id for tweet in tweets])
        df['len'] = np.array([len(tweet.full_text) for tweet in tweets])
        df['date'] = np.array([tweet.created_at for tweet in tweets])
        df['source'] = np.array([tweet.source for tweet in tweets])
        df['likes'] = np.array([tweet.favorite_count for tweet in tweets])
        df['retweets'] = np.array([tweet.retweet_count for tweet in tweets])
        df['lang'] = np.array([tweet.lang for tweet in tweets])
        df['source'] = np.array([tweet.source for tweet in tweets])
        df['created_at'] = np.array([tweet.created_at for tweet in tweets])
        df['tweets'] = np.array([tweet.full_text for tweet in tweets])

        return df


def login_page(request):
    args = {}
    try:
        twitter_token = request.COOKIES['twitter_token']
    except:
        twitter_token = None
    if twitter_token:
        return redirect('home')
    else:
        pass
    if request.POST:
        return OAuth(request)
    else:
        try:
            logout = request.session["logout"]
        except:
            logout = None
        if logout is not None:
            args['message'] = 'Successfully logged out!'
        else:
            pass
        return render(request, 'main/login.html', args)


def OAuth(request):
    client = UserClient(CONSUMER_KEY, CONSUMER_SECRET)
    token = client.get_authorize_token(CALLBACK_URL)
    request.session["oauth_token"] = token.oauth_token
    request.session["oauth_token_secret"] = token.oauth_token_secret
    return redirect(token.auth_url)


def OAuth2CallBack(request):
    ACCESS_TOKEN = request.session['oauth_token']
    ACCESS_TOKEN_SECRET = request.session['oauth_token_secret']
    OAUTH_VERIFIER = request.GET['oauth_verifier']
    client = UserClient(CONSUMER_KEY, CONSUMER_SECRET,
                        ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    token = client.get_access_token(OAUTH_VERIFIER)

    response = redirect('/')
    response.set_cookie('twitter_token', token)
    response.set_cookie('access_token', token.oauth_token)
    response.set_cookie('access_token_secret', token.oauth_token_secret)
    return response


def logout(request):
    response = HttpResponseRedirect('/login/')
    response.delete_cookie('twitter_token')
    response.delete_cookie('access_token')
    response.delete_cookie('access_token_secret')
    request.session["logout"] = 'logout'
    return response
