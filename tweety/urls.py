"""tweety URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from core import views as core_views

urlpatterns = [
	path('admin/', admin.site.urls),
	path('login/', core_views.login_page, name="login"),
	path('login/twoauth2callback', core_views.OAuth2CallBack),
	path('logout/', core_views.logout),
	path('search/', core_views.search),
	path('visualize/', core_views.visualize),
	path('schedule/', core_views.schedule_page),
	path('schedule-realise/', core_views.new_tweet),
	path('', core_views.home_page, name="home"),
	path('<page>/', core_views.home_page, name="home"),
]
